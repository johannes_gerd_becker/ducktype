#pragma once

#include "../metaprog/fundamentals.h"
#include <type_traits>

template <class ...> using void_t = void;

struct WithTwoFs { void f () const; void f (); };

struct WithConstF { void f () const;  };
struct WithFConstRef { void f () const &; };
struct WithFRRef { void f () && ; };

template <class T, class = void> struct HasMultipleF { static constexpr bool value = true; };
template <class T> struct HasMultipleF<T,void_t<
  decltype(&T::f)
  >> {
  static constexpr bool value = false;

  template <class> struct DetermineTypeImpl;
  template <class S> struct DetermineTypeImpl<void (S::*) () const> { static constexpr int which = 1; };
  template <class S> struct DetermineTypeImpl<void (S::*) () const &> { static constexpr int which = 2; };
  template <class S> struct DetermineTypeImpl<void (S::*) () &&> { static constexpr int which = 3; };

  static constexpr int which = DetermineTypeImpl<decltype(&T::f)>::which;
};

static_assert(!HasMultipleF<WithConstF>::value && HasMultipleF<WithConstF>::which == 1, "");
static_assert(!HasMultipleF<WithFConstRef>::value && HasMultipleF<WithFConstRef>::which == 2, "");
static_assert(!HasMultipleF<WithFRRef>::value && HasMultipleF<WithFRRef>::which == 3, "");

template<typename T>
struct HasConstFMethod
{
  template<typename U, void (U::*)() const> struct SFINAE {};
  template<typename U> static char Test (SFINAE<U, &U::f>*);
  template<typename U> static int Test (...);
  static const bool Has = sizeof (Test<T> (0)) == sizeof (char);
};




template<typename T>
struct HasNonConstFMethod
{
  template<typename U, void (U::*)()> struct SFINAE {};
  template<typename U> struct wrap {};

  template<class U, class = void> struct HasF : std::false_type {};
  template<class U> struct HasF<U, void_t< decltype(std::declval<U>().f ()) >> : std::true_type {};

  template<class> struct sfinae {};
  template<class U, class R> static constexpr std::true_type test2 (R (U::*) ()) { return {}; }
  static constexpr std::false_type test2 (...) { return {}; }

  template<typename U> static constexpr std::true_type Test (SFINAE<U, &U::f>*) { return{}; }
  template<typename U> static constexpr std::false_type Test (...) { return{}; }

  template<typename U> static constexpr auto test (std::true_type) { return test2 (&U::f); }
  template<typename U> static constexpr auto test (std::false_type) { return std::false_type {}; }


  static constexpr bool Has = test<T> (HasF<T> {});
};

static_assert(HasConstFMethod<WithTwoFs>::Has, "");
static_assert(HasNonConstFMethod<WithTwoFs>::Has, "");
static_assert(!HasNonConstFMethod<WithConstF>::Has, "");
//static_assert(same<WithFConstRef> (), "");



template<typename T>
struct CloneAnalyzer {
private:
  // technique:
  //   http://stackoverflow.com/questions/87372/check-if-a-class-has-a-member-function-of-a-given-signature
  template <class U,
    auto (U::*) () const -> decltype ( std::declval<U const &> ().clone () ) >
  struct sfinae_const {};

  template <class U,
    auto (U::*) () const & -> decltype(std::declval<U const &> ().clone ()) >
  struct sfinae_const_ref {};

  template <class U, 
    auto (U::*) () && -> decltype(std::declval<U> ().clone ()) >
  struct sfinae_rref {};

  template <typename U>
  static constexpr std::true_type detect_clone_const (sfinae_const<U, &U::clone> *) { return{}; }
  template <typename U>
  static constexpr std::false_type detect_clone_const (...) { return{}; }

  template <typename U>
  static constexpr std::true_type detect_clone_const_ref (sfinae_const_ref<U, &U::clone> *) { return{}; }
  template <typename U>
  static constexpr std::false_type detect_clone_const_ref (...) { return{}; }

  template <typename U>
  static constexpr std::true_type detect_clone_rref (sfinae_rref<U, &U::clone> *) { return{}; }
  template <typename U>
  static constexpr std::false_type detect_clone_rref (...) { return{}; }

public:
  static constexpr bool has_clone_const = detect_clone_const<T> (0);
  static constexpr bool has_clone_const_ref = detect_clone_const_ref<T> (0);
  static constexpr bool has_clone_rref = detect_clone_rref<T> (0);
};

struct NoClone{};
struct CloneConst { CloneConst * clone () const; };
struct MoveCloneOnly { MoveCloneOnly * clone () &&; };
struct TwoClones { TwoClones * clone () &&; CloneConst * clone () const &; };

//static_assert(CloneAnalyzer<CloneConst>::HasConstClone<CloneConst>{}, "");

static_assert(!CloneAnalyzer<NoClone>::has_clone_const, "");
static_assert(!CloneAnalyzer<NoClone>::has_clone_const_ref, "");
static_assert(!CloneAnalyzer<NoClone>::has_clone_rref, "");

static_assert(CloneAnalyzer<CloneConst>::has_clone_const, "");
static_assert(!CloneAnalyzer<CloneConst>::has_clone_const_ref, "");
static_assert(!CloneAnalyzer<CloneConst>::has_clone_rref, "");

static_assert(!CloneAnalyzer<MoveCloneOnly>::has_clone_const, "");
static_assert(!CloneAnalyzer<MoveCloneOnly>::has_clone_const_ref, "");
static_assert(CloneAnalyzer<MoveCloneOnly>::has_clone_rref, "");

static_assert(!CloneAnalyzer<TwoClones>::has_clone_const, "");
static_assert(CloneAnalyzer<TwoClones>::has_clone_const_ref, "");
static_assert(CloneAnalyzer<TwoClones>::has_clone_rref, "");

//static_assert(ConstRefDetector<decltype(&WithFConstRef::f)>::value, "");
/*
struct ClassWithoutClone {};
struct ClassWithClone { virtual void * clone (); };

template <class B>
struct CloneOverride : B {
  void * clone () override { return nullptr; }; constexpr static bool value = true;
};

template <class T>
constexpr auto tryInstantiate () {
  auto t = CloneOverride<T>{}.value;
  return true;
}


template <class T, class = void> struct HasClone { constexpr static bool value = false; };
template <class T> struct HasClone<T, void_t<
  decltype(CloneOverride<T>{}.value)
  >> { constexpr static bool value = true; };


static_assert(HasClone<ClassWithClone>::value, "");
static_assert(!HasClone<ClassWithoutClone>::value, "");
*/