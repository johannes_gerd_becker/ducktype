#include "../metaprog/multifunction.h"
#include "tests.h"

// clang 3.5 workaround
void gets();

#include <iostream>

namespace metaprog { namespace tests {
  static auto e = ([] () {
    add_simple_test ("Testing combine", []() {
      auto add = metaprog::combine (
        [](int x) { return x + 1; },
        [](auto && x) { return std::forward<decltype(x)> (x) + 2; }
      );
      std::cout << add (7) << "\n";
      std::cout << add (7.0) << "\n";
    });
	  return 0;
  }) ();
}}