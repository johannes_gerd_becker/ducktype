// tests.cpp : Defines the entry point for the console application.
//

#include "tests.h"
#include "../metaprog/fundamentals.h"
#include "../visualcpp/discover_clone.h"
#include <cassert>
#include <iostream>
#include <vector>

namespace metaprog { namespace tests {

  using test_list_type = std::vector<std::unique_ptr<itest>>;

  static test_list_type & get_test_list () {
    static auto test_list = test_list_type{};
    return test_list;
  }

  void add_test (std::unique_ptr<itest> test) {
    get_test_list().push_back (std::move (test));
  }

  void run_tests () {
    for (auto const & test : get_test_list ()) {
      std::cout << "Running test: " << test->get_name () << "\n";
      test->run ();
    }
  }


}}


int main () {
  using namespace metaprog::tests;

  run_tests ();

  return 0;
}
