#pragma once

#include <memory>
#include <string>

namespace metaprog { namespace tests {

  class itest {
  public:
    itest (std::string name) : name (std::move (name)) {}
    virtual ~itest () = default;

    std::string get_name () const { return name; }
    virtual void run () const = 0;

  private:
    std::string name;
  };

  namespace detail {
    template <class F>
    class simple_test : public itest {
    public:
      explicit simple_test (std::string name, F f) : itest(std::move(name)), f(f) { }

      void run () const override {
        f ();
      }

    private:
      F f;
    };
  }

  void add_test (std::unique_ptr<itest> test);
  void run_tests ();

  template <class F>
  std::unique_ptr<itest> make_simple_test (std::string name, F f) {
    return std::make_unique<detail::simple_test<F>> (std::move (name), f);
  }

  template <class F>
  void add_simple_test (std::string name, F f) {
    add_test (make_simple_test (std::move (name), f));
  }
}}
 