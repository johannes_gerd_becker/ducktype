#include "../metaprog/type_operations.h"
#include <cassert>
#include "tests.h"

namespace metaprog { namespace tests {

  namespace {

    template<class T>
    struct MyTemplatedBase {};

    template<class T, class S>
    struct MyTwoTypesTemplatedBase {};

    template <class D, class B>
    struct MyDecoration : B {
      using B::B;
      static constexpr int a = 42;
    };

    template <class D>
    struct MyCrtpTemplateBase {
      int getW () const { return static_cast<D const *> (this)->w; }
    };

    template <class D, class B>
    struct MyCrtpDecoration : B {
      using B::B;
      int w = 20;
    };

    template <class> struct TemplateA {};
    template <class> struct TemplateB {};
  }

  static int e = ([]() {

    //
    // Testing: DecorateCrtp
    //

    // Decorate a class
    {
      struct MyBase {};
      // variable needed to make VC++ 15 happy
      auto decorated_type = metaprog::decorate (metaprog::type <MyBase>{}, metaprog::type_template<MyDecoration> {});;
      constexpr metaprog::untype<decltype(decorated_type)> b{};
      static_assert (b.a == 42, "Type_operations: DecorateCrtp: Decorating a class failed");
    }

    /*
    // Decorate a template
    {
      struct MyBase {};
      auto tplt_decorated_type2 = metaprog::apply_operations (metaprog::type_template <MyTemplatedBase>{}, DecorateCrtp<MyDecoration> {});
      using tplt_decorated2 = decltype (tplt_decorated_type2);
      constexpr metaprog::untype_template<tplt_decorated2, int> b2{};
      static_assert (b2.a == 42, "Type_operations: DecorateCrtp: Decorating a class template failed");
    }
    */

    //
    // Testing: SpecializeAll
    //
    {
      auto tplt_specialized_type = metaprog::apply_operations (metaprog::type_template <MyTemplatedBase>{},
        SpecializeAll<int> {});
      using tplt_specialized = decltype (tplt_specialized_type);
      static_assert (std::is_same<metaprog::untype<tplt_specialized>, MyTemplatedBase<int>>::value,
        "Type_operations: SpecializeAll: Specializing a class template failed");
    }


    /*
    //
    // Testing: ConcludeCrtp
    //
    add_simple_test ("Type_operations: ConcludeCrtp", [] {
      auto crtp_decorated_type = metaprog::apply_operations (metaprog::type_template <MyCrtpTemplateBase>{},
        DecorateCrtp<MyCrtpDecoration> {},
        ConcludeCrtp{});
      using crtp_decorated = decltype (crtp_decorated_type);
      metaprog::untype<crtp_decorated> b_crtp{};
      assert (b_crtp.getW () == 20);
    });
    */

    {
      auto transformed_type = metaprog::apply_operations (type<int>{}, Identity{});
      using transformed = decltype (transformed_type);
      using untyped = untype<transformed>;
      static_assert (std::is_same<untyped, int>::value, "Type_operations: Identity: Applying to class failed");
    }

    {
      auto transformed_type = metaprog::apply_operations (type_template<MyTemplatedBase>{}, Identity{});
      using transformed = decltype (transformed_type);
      using untyped = untype_template<transformed, int>;
      static_assert (std::is_same<untyped, MyTemplatedBase<int>>::value,
        "Type_operations: Identity: Applying to class template failed");
    }

    {
      auto partially_specialized_type = metaprog::apply_operations (type_template<MyTwoTypesTemplatedBase>{},
        SpecializeFromRight<char> {});
      using partially_specialized = decltype (partially_specialized_type);
      using untyped = untype_template<partially_specialized, int>;
      static_assert (std::is_same<untyped, MyTwoTypesTemplatedBase<int, char>>::value,
        "Type_operations: SpecializeFromRight: Applying to class template failed");
    }


    // TODO: move to test_fundamentals.cc
    {
      using metaprog::template_list;
      static_assert (length (template_list<> {}) == 0, "");
      static_assert (length (template_list<TemplateA, TemplateB> {}) == 2, "");

      static_assert (head (template_list<TemplateA, TemplateB> {}) == type_template<TemplateA>{}, "");

    }

    {
      using metaprog::template_list;
      using metaprog::apply_operations;
      static_assert (head (apply_operations (template_list<> {}, Append<TemplateA> {})) == type_template<TemplateA>{}, "");
      static_assert (head (apply_operations (template_list<> {}, Append<TemplateA> {}, Append<TemplateB> {}))
        == type_template<TemplateA>{}, "");
      static_assert (head (tail(apply_operations (template_list<> {}, Append<TemplateA> {}, Append<TemplateB> {})))
        == type_template<TemplateB>{}, "");
    }
    return 0;
  }) ();


}}