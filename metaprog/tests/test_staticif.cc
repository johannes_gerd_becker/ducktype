#undef NDEBUG

#include "../metaprog/staticif.h"

// clang 3.5 workaround
void gets();

#include "tests.h"
#include <memory>
#include <sstream>
#include <tuple>
#include <cassert>


namespace metaprog { namespace tests {

  namespace {
    struct is_pointer_t {
      template <class T> constexpr std::true_type operator () (T *) const { return{}; }
      template <class T> constexpr std::false_type operator () (T) const { return{}; }
    };

    struct is_int_t {
      constexpr std::true_type operator () (int) const { return{}; }
      template <class T> constexpr std::false_type operator () (T) const { return{}; }
    };

    struct is_double_t {
      constexpr std::true_type operator () (double) const { return{}; }
      template <class T> constexpr std::false_type operator () (T) const { return{}; }
    };

    template<class T>
    struct static_const
    {
      constexpr static T const value{};
      constexpr T const & operator () () const { return value; }
    };

    template<class T>
    constexpr T const static_const<T>::value;

    constexpr static auto const & is_pointer = static_const<is_pointer_t>{} ();
    constexpr static auto const & is_int = static_const<is_int_t>{} ();
    constexpr static auto const & is_double = static_const<is_double_t>{} ();





    template <class Head, class ... Tail>
    std::string params_to_string (Head head, Tail ... tail) {
      using metaprog::static_if;
      using metaprog::bool_c;

      std::stringstream s{};
      s << head;
      static_if <(sizeof ... (tail) > 0)> (
        [&](auto, auto ... nothing) {
        s << ", ";
        s << params_to_string (tail ..., nothing ...);
      },

        [&](auto) {
        s << "\n";
      }
      );
      return s.str ();
    }
  }

  static int e = ([]() {
    add_simple_test ("Static_if: If and else branch", []() {
      enum alternatives { cPtr, cInt, cNeither };

      auto lambda = [](auto x) {
        return metaprog::static_if (
          is_pointer (x),
          [&](auto use) {
          return std::make_tuple (cPtr, *use (x));
        },

          is_int (x),
          [&](auto use) {
          return std::make_tuple (cInt, use (x));
        },

          [&](auto) {
          return cNeither;
        });
      };

      int x = 5;
      auto y = &x;
      double z = 7.0;

      assert (lambda (x) == std::make_tuple (cInt, x));
      assert (lambda (y) == std::make_tuple (cPtr, *y));
      assert (lambda (z) == cNeither);
    });

    add_simple_test ("Static_if: If branch only", []() {
      auto lambda = [](auto x) {
        return metaprog::static_if (
          is_double (x),
          [&](auto use) {
            return use (x);
          }
        );
      };

      int x = 5;
      double z = 7.0;

      assert (lambda (z) == z);
      assert ((std::is_same<void, decltype(lambda (x))>::value));
    });

    add_simple_test ("Static_if: Use with bool_c condition", []() {
      assert (static_if (bool_c <sizeof (char) == 1> (), [&](auto) { return 42; }) == 42);
    });

    add_simple_test ("Static_if: Use with bool condition", []() {
      assert (static_if <sizeof (char) == 1> ([&](auto) { return 42; }) == 42);
    });

    add_simple_test ("Static_if: Use case where both parameters in callback are needed", []() {
      assert (params_to_string (1, 2, 3, 4) == "1, 2, 3, 4\n");
      assert (params_to_string (1) == "1\n");
    });

    {
      struct dummy { constexpr bool v () const { return true; } };
      static_assert(static_ternary (std::true_type{}, 1, dummy{}) == 1, "");
      static_assert(static_ternary (std::false_type{}, 1, dummy{}).v (), "");
      static_assert(static_ternary <true> (1, dummy{}) == 1, "");
      static_assert(static_ternary <false> (1, dummy{}).v (), "");
    }

    add_simple_test ("Staticif: static_ternary preserves lvalue references", []() {
      int x = 0, y = 0;
      int & x1 = static_ternary (std::true_type{}, x, y);
      int & y1 = static_ternary (std::false_type{}, x, y);
      assert (&x1 == &x);
      assert (&y1 == &y);
    });
    add_simple_test ("Staticif: static_ternary does not preserve rvalue references", []() {
      int x = 0, y = 0;
      int && x1 = static_ternary (std::true_type{}, std::move (x), std::move (y));
      int && y1 = static_ternary (std::false_type{}, std::move (x), std::move (y));
      assert (&x1 != &x);
      assert (&y1 != &y);
    });

    return 0;
  }) ();

} }