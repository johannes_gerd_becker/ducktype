#pragma once

#include "../metaprog/fundamentals.h"

namespace metaprog {

  // TypeOp: base class for operations on type < > values
  //   derived classes must define an ApplyTo nested template
  template <class Derived>
  struct TypeOp {
    template <class T>
    constexpr auto operator () (type<T>) const {
      return type< typename Derived::template ApplyTo<T> > {};
    }
  };

  template <class Derived>
  struct TemplateTypeOp {
    template <template <class ...> class T>
    constexpr auto operator () (type_template<T>) const {
      return type< typename Derived::template ApplyTo<T> > {};
    }
  };

  // AddInterfaceOp: add an interface to a class by deriving from both the class and the interface,
  //   inheriting all constructors of the class
  template <class I>
  struct AddInterfaceOp
    : TypeOp< AddInterfaceOp<I> >
  {
    template <class B>
    struct ApplyTo : B, I {
      using B::B;
    };
  };

  template <class I>
  constexpr auto add_interface (type<I>) -> AddInterfaceOp<I> {
    return{};
  }


  template<class D, class B, template <class ...> class ... Ts> struct CombinedDecoration;

  template<class D, class B, template <class ... > class T, template <class ...> class ... Ts>
  struct CombinedDecoration<D, B, T, Ts ...>
    : T<D, CombinedDecoration<D, B, Ts ...>>
  {
    using base = T<D, CombinedDecoration<D, B, Ts ...>>;
    using base::base;
  };

  template<class D, class B>
  struct CombinedDecoration<D, B> : B {
    using B::B;
  };

  // Requires:
  //   Decoration: Type * Type -> Type, (D, B) => g (D, B)
  // Provides:
  //   operator (): [T] => [D => g (D, T)]
  //   operator (): [P => f(P)] => [D => g(D, f(D))]
  template <template<class ...> class ... Decorations>
  struct Decorate {
  private:
    template <class P>
    struct result : CombinedDecoration<result<P>, P, Decorations ...> {
      using base = CombinedDecoration<result<P>, P, Decorations ...>;
      using base::base;
    };

  public:
    template <class T>
    constexpr auto operator () (metaprog::type<T>) const {
      using result_ = result<T>;
      return metaprog::type < result_ > {};
    }
  };

  template<class B, template <class ...> class T>
  constexpr auto decorate (type<B>, type_template<T>) {
	  struct Result : T<Result, B> { using T<Result, B>::T; };
	  return metaprog::type<Result> {};
  }

  template <class T, template <class ...> class ... Decorations>
  constexpr auto decorate_type (type<T> t, metaprog::template_list<Decorations ...>) {
    return Decorate<Decorations ...> {} (t);
  }




  template <class ... P>
  struct SpecializeAll {
    template <template <class ...> class T>
    constexpr auto operator () (metaprog::type_template<T>) const {
      using result = T<P ...>;
      return metaprog::type < result > {};
    }
  };

  template <class ... P>
  struct SpecializeFromRight {
  private:
    template <template <class ...> class T>
    struct result {
      template <class ... Q> using type = T<Q..., P...>;
    };

  public:
    template <template <class ...> class T>
    constexpr auto operator () (metaprog::type_template<T>) const {
      return metaprog::type_template < result<T>::template type > {};
    }
  };


  template <class ... P>
  struct SpecializeAllButFirst {
    template <template <class ...> class T>
    struct Specialized {
      template <class D>
      using type = T<D, P ...>;
    };

    template <template <class ...> class T>
    constexpr auto operator () (type_template<T>) const {
      using specialized = Specialized<T>;
      return type_template<specialized::template type> {};
    }
  };



  struct ConcludeCrtp {
    template <template <class> class B>
    struct Crtp : B < Crtp<B> > { using base = B<Crtp>; using base::base; };

    template <template <class> class T>
    constexpr auto operator () (metaprog::type_template<T>) const {
      return metaprog::type <Crtp <T>> {};
    }
  };

  struct Identity {
    template <class T> constexpr T operator () (T t) const { return std::move(t); }
  };

  template <template <class ...> class T>
  struct Append {
    template <template <class ...> class ... Ts>
    constexpr metaprog::template_list<Ts ..., T> operator () (metaprog::template_list<Ts ...>) const {
      return{};
    }
  };




}