#ifndef METAPROG_STATICIF_H_JSQ3W9OTOXZD2M7PLDQO
#define METAPROG_STATICIF_H_JSQ3W9OTOXZD2M7PLDQO

#pragma once

#include "metaprog/fundamentals.h"
#include <type_traits>
#include <utility>

namespace metaprog {
	
	
	
	//
	// static_if
	//
	
	namespace detail {
		template <class F>
		decltype(auto) static_if_branch_call_ (F && f) {
			return std::forward<F> (f) (
				// a no-op generic lambda
				[] (auto && x) -> decltype(auto) { return std::forward<decltype(x)> (x); }	
			);
		}
	}
	
	// two-argument version
	
	template <class F>
	decltype(auto) static_if (std::true_type, F && f) {
		return detail::static_if_branch_call_ (std::forward<F> (f));
	}	
	
	template <class F>
	void static_if (std::false_type, F &&) {
	}
	
	// three-argument version
	
	template <class F, class G>
	decltype(auto) static_if (std::true_type, F && f, G &&) {
		return detail::static_if_branch_call_ (std::forward<F> (f));
	}	
	
	template <class F, class G>
	decltype(auto) static_if (std::false_type, F &&, G && g) {
		return detail::static_if_branch_call_ (std::forward<G> (g));
	}

	// four-or-more-argument version

	template <class F, bool B, class Other, class ... Others>
	decltype(auto) static_if (std::true_type, F && f, std::integral_constant<bool,B>, Other &&, Others && ...) {
		return detail::static_if_branch_call_ (std::forward<F> (f));
	}

	template <class F, bool B, class Other, class ... Others>
	decltype(auto) static_if (std::false_type, F &&, std::integral_constant<bool,B> b, Other && other, Others && ... others) {
		return static_if (b, std::forward<Other> (other), std::forward<Others> (others) ...);
	}

	// angular brackets version: static_if <bool (or integer)> (if_lambda, else_lambda)
	
	template <long long B, class ... Args>
	decltype(auto) static_if (Args && ... args) {
		return static_if (bool_c <B> (), std::forward<Args> (args) ...);
	}

  // static_ternary

  template <class IfTy, class ElseTy>
  constexpr IfTy static_ternary (std::true_type, IfTy && i, ElseTy &&) {
    return std::forward<IfTy> (i);
  }
	
  template <class IfTy, class ElseTy>
  constexpr ElseTy static_ternary (std::false_type, IfTy &&, ElseTy && e) {
    return std::forward<ElseTy> (e);
  }

  // angular brackets version

  template <long long B, class ... Args>
  constexpr decltype(auto) static_ternary (Args && ... args) {
    return static_ternary (bool_c <B> (), std::forward<Args> (args) ...);
  }
}

#endif