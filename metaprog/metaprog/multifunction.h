#ifndef METAPROG_MULTIFUNCTION_H_JSQ3W9OTOXZD2M7PLDQO
#define METAPROG_MULTIFUNCTION_H_JSQ3W9OTOXZD2M7PLDQO

#include <utility>

namespace metaprog {

	namespace detail {
		
		template <class ... T> struct Combined : T ... {
			explicit Combined (T ... t) : T{ std::move (t) } ... {}
			using T::operator () ...;
		};

	}

	template <class ... T>
	auto combine (T ... t) {
		return detail::Combined<T ...> {std::move (t) ...};
	}

}

#endif

