// enriched.cpp : Defines the entry point for the console application.
//

#include "duck/implement.h"
#include <iostream>
#include <memory>

struct elephant {
	void draw() const { std::cout << "drawing an elephant" << " color: " << color << std::endl; }
  void set_color (int c) { color = c; }
  int color = 0;
};

class idrawable {
public:
  virtual void draw() const = 0;
  virtual ~idrawable() = default;

  using interface = idrawable;
  template <class T, class D, class B> struct implementation;
};

template<class T, class D, class B>
struct idrawable::implementation : B {
  using B::B;
  void draw () const override {
    auto self = ducktype::inner (this);
    self->draw ();
  }
};


void drawtwice_impl(idrawable const & d) {
  std::cout << "drawing twice: " << std::endl;
  d.draw();
  d.draw();
}

template <class T>
void drawtwice (T && t) {
  drawtwice_impl (ducktype::as_ref<idrawable const> (t));
}



int run_demo() {  
  using ducktype::implement;
  using ducktype::as_ref;

  auto as_drawable = [](auto && t) { return ducktype::as_ref<idrawable> (t); };


	as_drawable (elephant {}).draw();
  /*
  std::cout << "Size of drawable elephant: " << sizeof(drawable( elephant{} )) << std::endl;
  std::cout << "Max embedded: " << drawable (elephant{}).max_size_embedded << std::endl;
  std::cout << "Embeds? " << drawable (elephant{}).embeds () << std::endl;
  */

  auto e = elephant{};
  
  /*std::cout << "Size of implementation: " << sizeof (ducktype::implement<idrawable> (std::ref (e))) << "\n";
  */

  as_drawable (e).draw ();
  e.set_color (1);
  as_drawable (e).draw ();
  
  auto a = implement<idrawable> ( elephant {} );
  a.draw();
  auto b = a;
  b.draw();

  drawtwice(elephant{});
  drawtwice(a);

  auto q = std::make_unique<elephant>();
  auto r = implement<idrawable>(std::move(q));
  r.draw();

  return 0;
  
}

