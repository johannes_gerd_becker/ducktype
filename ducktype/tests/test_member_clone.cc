#include "tests.h"

#include "metaprog/type_operations.h"
#include "duck/member_clone.h"
#include <cstddef>

namespace ducktype { namespace tests {
  namespace {




    static std::nullptr_t e = ([]() {

      {
        struct NoClone {};
        struct CloneConst { CloneConst * clone () const; };
        struct MoveCloneOnly { MoveCloneOnly * clone () && ; };
        struct TwoClones { TwoClones * clone () && ; TwoClones * clone () const &; };

        static_assert(!CloneAnalyzer<NoClone>::has_clone_const, "");
        static_assert(!CloneAnalyzer<NoClone>::has_clone_const_ref, "");
        static_assert(!CloneAnalyzer<NoClone>::has_clone_rref, "");

        static_assert(CloneAnalyzer<CloneConst>::has_clone_const, "");
        static_assert(!CloneAnalyzer<CloneConst>::has_clone_const_ref, "");
        static_assert(!CloneAnalyzer<CloneConst>::has_clone_rref, "");

        static_assert(!CloneAnalyzer<MoveCloneOnly>::has_clone_const, "");
        static_assert(!CloneAnalyzer<MoveCloneOnly>::has_clone_const_ref, "");
        static_assert(CloneAnalyzer<MoveCloneOnly>::has_clone_rref, "");

        static_assert(!CloneAnalyzer<TwoClones>::has_clone_const, "");
        static_assert(CloneAnalyzer<TwoClones>::has_clone_const_ref, "");
        static_assert(CloneAnalyzer<TwoClones>::has_clone_rref, "");
      }

      {
        using std::size_t;

        struct NoPlmtClone {};
        struct PlmtCloneConst { PlmtCloneConst * clone (void *, size_t, size_t) const; };
        struct PlmtMoveCloneOnly { PlmtMoveCloneOnly * clone (void *, size_t, size_t) && ; };
        struct PlmtTwoClones {
          PlmtTwoClones * clone (void *, size_t, size_t) &&;
          PlmtTwoClones * clone (void *, size_t, size_t) const &;
        };

        static_assert(!CloneAnalyzer<NoPlmtClone>::has_placement_clone_const, "");
        static_assert(!CloneAnalyzer<NoPlmtClone>::has_placement_clone_const_ref, "");
        static_assert(!CloneAnalyzer<NoPlmtClone>::has_placement_clone_rref, "");

        static_assert(CloneAnalyzer<PlmtCloneConst>::has_placement_clone_const, "");
        static_assert(!CloneAnalyzer<PlmtCloneConst>::has_placement_clone_const_ref, "");
        static_assert(!CloneAnalyzer<PlmtCloneConst>::has_placement_clone_rref, "");

        static_assert(!CloneAnalyzer<PlmtMoveCloneOnly>::has_placement_clone_const, "");
        static_assert(!CloneAnalyzer<PlmtMoveCloneOnly>::has_placement_clone_const_ref, "");
        static_assert(CloneAnalyzer<PlmtMoveCloneOnly>::has_placement_clone_rref, "");

        static_assert(!CloneAnalyzer<PlmtTwoClones>::has_placement_clone_const, "");
        static_assert(CloneAnalyzer<PlmtTwoClones>::has_placement_clone_const_ref, "");
        static_assert(CloneAnalyzer<PlmtTwoClones>::has_placement_clone_rref, "");

        static_assert(!CloneAnalyzer<PlmtTwoClones>::has_clone_const, "");
        static_assert(!CloneAnalyzer<PlmtTwoClones>::has_clone_const_ref, "");
        static_assert(!CloneAnalyzer<PlmtTwoClones>::has_clone_rref, "");
      }

      {
        struct icloneable { virtual icloneable * clone () const = 0; };
        struct MyBase {};
        struct MyExtended : MyBase, icloneable {  };

        static_assert (CloneAnalyzer<MyExtended>::has_clone_const, "");

        auto extended_type = metaprog::apply_operations (metaprog::type<MyBase>{}, metaprog::AddInterfaceOp<icloneable> {});
        auto implementation_type = metaprog::decorate_type (extended_type,
          determine_clone_member_decorations (extended_type)
        );
        using implementation = metaprog::untype <  decltype( implementation_type ) > ;
        implementation a{};
      }

      {
        struct MyInterface {
          virtual int f () = 0;
          virtual ~MyInterface () = default;
        };
        using extended = icopy_move_extended<CopyMove::Both, MyInterface>;
        struct Impl : extended {
          int f () override { return 45; }
          extended * clone () const & override { return nullptr; }
          extended * clone (void *, std::size_t, std::size_t) const & override { return nullptr; }
          extended * clone () && override { return nullptr; }
          extended * clone (void *, std::size_t, std::size_t) && override { return nullptr; }
        };
        Impl a;
        // if this compiles, all clone functions are present in the extended interface

        static_assert (CloneAnalyzer<extended>::has_clone_const_ref, "");
        static_assert (CloneAnalyzer<extended>::has_clone_rref, "");
      }

      {
        struct MyInterface {
          virtual int f () = 0;
          virtual ~MyInterface () = default;
        };
        using extended = icopy_move_extended<CopyMove::NoCopy, MyInterface>;
        struct Impl : extended {
          int f () override { return 45; }
          extended * clone () && override { return nullptr; }
          extended * clone (void *, std::size_t, std::size_t) && override { return nullptr; }
        };
        Impl a;
        // if this compiles, only the move clone functions are present in the extended interface
      }

      {
        struct MyInterface {
          virtual int f () = 0;
          virtual ~MyInterface () = default;
        };
        using extended = icopy_move_extended<CopyMove::None, MyInterface>;
        struct Impl : extended {
          int f () override { return 45; }
        };
        Impl a;
        // if this compiles, no clone functions are present in the extended interface
      }


      return nullptr;
    }) ();


  }
}}

