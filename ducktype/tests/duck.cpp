// enriched.cpp : Defines the entry point for the console application.
//

//#include "ducktype/duck.h"
#include "duck/implement.h"
#include "duck/polymorphic_value.h"
#include <iostream>
#include <memory>

struct elephant{
  //elephant(elephant const &) = delete;
  //elephant(elephant &&) = default;
	void draw() const { std::cout << "drawing an elephant" << " color: " << color << std::endl; }
  void set_color (int c) { color = c; }
  int color = 0;
  //int ary[100];
};

class idrawable {
public:
  virtual void draw() const = 0;
  virtual ~idrawable() = default;
};


template<class T>
struct ducktype::implementation_traits<idrawable, T> {
  template <class D, class B>
  class extension : public B
  {
  public:
    using B::B;
    void draw () const override { this->obj ().draw (); }
  };
};

using drawable_t = ducktype::polymorphic_value<ducktype::detail::ProduceInterface<idrawable,ducktype::CloneMove::Both>>;

template <class T>
drawable_t drawable(T t) {
  auto a = ducktype::implement<idrawable>(t);
  return drawable_t{ a };
}

void drawtwice(idrawable const & d) {
  std::cout << "drawing twice: " << std::endl;
  d.draw();
  d.draw();
}

int run_demo()
{
  /*
	drawable( elephant {} )->draw();
  std::cout << "Size of drawable elephant: " << sizeof(drawable( elephant{} )) << std::endl;
  
  //drawable( std::make_shared<elephant>() )->draw();
  //std::cout << "Size of shared ptr elephant: " << sizeof(std::shared_ptr<elephant>) << std::endl;
  
  auto e = elephant{};
  auto de = drawable (std::ref (e));
  de->draw ();
  e.set_color (1);
  de->draw ();
  
  auto a = drawable( elephant {} );
  a->draw();
  auto b = a;
  b->draw();

  drawtwice(drawable(elephant{}));
  drawtwice(a);
  //auto c = drawable(b);
  
//  auto p = ducktype::construct_implement<idrawable, elephant> ();
//  p.draw();

  auto q = std::make_unique<elephant>();
  auto r = ducktype::implement<idrawable>(std::move(q));
  r.draw();

  std::cout << ducktype::detail::is_dereferencable<int *>::value << std::endl;
  std::cout << ducktype::detail::is_dereferencable<int>::value << std::endl;
//  std::cout << ducktype::detail::is_cloneable<ducktype::ienriched<idrawable,ducktype::CloneMove::Both>>::value << std::endl;
//  std::cout << ducktype::detail::is_cloneable<ducktype::ienriched<idrawable,ducktype::CloneMove::NoClone>>::value << std::endl;
  */
  return 0;
}

