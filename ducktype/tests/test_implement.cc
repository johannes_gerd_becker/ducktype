#include "duck/implement.h"
#include <cassert>
#include <cstddef>
#include "tests.h"

namespace ducktype { namespace tests {

  namespace {
    struct simple_pair {
      simple_pair (int x, int y) : x (x), y (y) {}
      //simple_pair (simple_pair const &) = default;
      int x, y;
    };

    template <class D, class B>
    struct sum_decoration : B {
      using B::B;

      int get_sum () const {
        auto self = ducktype::inner (this);
        return self->x + self->y;
      }
    };

    struct ifooable {
      virtual int foo () const = 0;
      virtual ~ifooable () = default;
    };

    struct ifooable_implementation {
      using interface = ifooable;

      template <class T, class D, class B>
      struct implementation : B {
        using B::B;
        int foo () const override { return 42; }
      };
    };

    struct FortyTwoHater { int hate = 43; };

    template <class D, class B>
    struct ifooable_implementation::implementation<FortyTwoHater, D, B> : B {
      using B::B;
      int foo () const override { return inner(this)->hate; }
    };

    template <class D, class B>
    struct ifooable_implementation::implementation<int, D, B> : B {
      using B::B;
      int foo () const override { return *inner(this); }
    };

  }

  static std::nullptr_t e = ([]() {

    add_simple_test("adapt", [] () {
      simple_pair p = { 3, 2 };

      auto adapted = adapt<sum_decoration> (std::ref (p));
      assert (adapted.get_sum () == 5);
    });

    add_simple_test ("force_implement", []() {
      float y = 7;
      FortyTwoHater fth{};
      int i = 44;
      auto ip = std::make_unique<int> (45);

      auto implemented_y = force_implement<ifooable_implementation> (y);
      assert (implemented_y.foo () == 42);

      auto implemented_fth = force_implement<ifooable_implementation> (fth);
      assert (implemented_fth.foo () == 43);

      auto implemented_i = force_implement<ifooable_implementation> (i);
      assert (implemented_i.foo () == 44);

      auto implemented_ip = force_implement<ifooable_implementation> (std::move(ip));
      assert (implemented_ip.foo () == 45);
    });

#ifndef _MSC_VER

    add_simple_test ("decorate", []() {
      simple_pair p = { 3, 2 };

      auto decorated = decorate<sum_decoration> (p);
      assert (decorated.get_sum () == 5);

      decorated.y = 7;
      assert (decorated.get_sum () == 10);
    });

    add_simple_test ("adapt unique pointer to pair", []() {
      auto p = std::make_unique<simple_pair> (3, 2);
      auto adapted = adapt<sum_decoration> (std::move (p));
      assert (adapted.get_sum () == 5);
      assert ((* ducktype::outer (&adapted))->x == 3);
      assert ((* ducktype::outer (&adapted))->y == 2);
      (*ducktype::outer (&adapted))->x = 10;
      assert ((*ducktype::outer (&adapted))->x == 10);
      assert ((*ducktype::outer (&adapted))->y == 2);
      auto q = std::move (*ducktype::outer (&adapted));
      assert (q->x == 10);
      assert (q->y == 2);
    });

#endif


    return nullptr;
  }) ();

} }




