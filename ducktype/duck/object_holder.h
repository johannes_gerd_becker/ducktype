#pragma once

#include <type_traits>
#include <memory>
#include <functional>

namespace ducktype {

  struct access_directly {
    template <class X> decltype(auto) operator () (X && x) const { return std::forward<decltype(x)> (x); }
  };
  struct access_by_dereferencing {
    template <class X> decltype(auto) operator () (X && x) const { return *x; }
  };
  struct access_by_member_get {
    template <class X> decltype(auto) operator () (X && x) const { return x.get (); }
  };

  template<class T> struct access_traits { using functional = access_directly; };
  template<class T> struct access_traits<T *> { using functional = access_by_dereferencing; };
  template<class T, class D> struct access_traits<std::unique_ptr<T, D>> { using functional = access_by_dereferencing; };
  template<class T> struct access_traits<std::shared_ptr<T>> { using functional = access_by_dereferencing; };
  template<class T> struct access_traits<std::reference_wrapper<T>> { using functional = access_by_member_get; };
  template<class T> struct access_traits<T const> { using functional = typename access_traits<T>::functional; };
  template<class T> struct access_traits<T volatile> { using functional = typename access_traits<T>::functional; };

  struct object_holder_emplace_tag {};



  // object_holder
  template <class T, class AccessFunctional>
  class object_holder {
  public:
    template <class ... Args>
    object_holder (object_holder_emplace_tag, Args && ... args) : m_obj (std::forward<Args> (args) ...) {}

    friend auto * get_inner (object_holder<T, AccessFunctional> * holder) {
      return &(AccessFunctional{} (holder->m_obj));
    }

    friend auto const * get_inner (object_holder<T, AccessFunctional> const * holder) {
      return &(AccessFunctional{} (holder->m_obj));
    }

    friend auto * get_outer (object_holder<T, AccessFunctional> * holder) {
      return &holder->m_obj;
    }

    friend auto const * get_outer (object_holder<T, AccessFunctional> const * holder) {
      return &holder->m_obj;
    }

  private:
    T m_obj;
  };

  // is_convertible_to_object_holder_ptr
  template <class S, class AccessFunctional> constexpr std::true_type is_convertible_to_object_holder_ptr (object_holder<S, AccessFunctional> const *) { return{}; }
  constexpr std::false_type is_convertible_to_object_holder_ptr (...) { return{}; }


  // as_object_holder_ptr
  template <class S, class AccessFunctional>
  object_holder<S, AccessFunctional> const * as_object_holder_ptr (object_holder<S, AccessFunctional> const * p) {
    return p;
  }
  template <class S, class AccessFunctional>
  object_holder<S, AccessFunctional> * as_object_holder_ptr (object_holder<S, AccessFunctional> * p) {
    return p;
  }

  // inner
  template <class T>
  auto * inner (T * t) {
    if constexpr (decltype(is_convertible_to_object_holder_ptr (t)) {})
      return get_inner (as_object_holder_ptr (t));
    else
      return t;
  }
  

  // outer
  template <class T>
  auto * outer (T * t) {
    if constexpr (decltype(is_convertible_to_object_holder_ptr (t)) {})
      return get_outer (as_object_holder_ptr (t));
    else
      return t;
  }

}