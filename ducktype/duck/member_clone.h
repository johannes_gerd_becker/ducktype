#pragma once

#include "metaprog/fundamentals.h"
#include "metaprog/staticif.h"
#include "metaprog/type_operations.h"
#include <type_traits>


namespace ducktype {

  enum { kNone = -1, kCloneConst = 0, kCloneConstRef = 1, kCloneRref = 2, kPlacementCloneConst = 3, kPlacementCloneConstRef = 4, kPlacementCloneRref = 5 };

  template<typename T>
  struct CloneAnalyzer {
  private:
    // technique:
    //   http://stackoverflow.com/questions/87372/check-if-a-class-has-a-member-function-of-a-given-signature

    template <class R> struct CloneExistence : std::true_type { using return_type = R; };
    struct CloneNonExistence : std::false_type { using return_type = void*; };

    template <class R, class U> static auto test_clone_const_signature (R (U::*) () const)
      -> CloneExistence<R>;
    template <class U> static auto test_clone_const (std::nullptr_t)
      -> decltype(test_clone_const_signature (&U::clone));
    template <class U> static auto test_clone_const (...) -> CloneNonExistence;

    using findings_clone_const = decltype(test_clone_const<T> (nullptr));

    template <class R, class U> static auto test_clone_const_ref_signature (R (U::*) () const &)
      ->CloneExistence<R>;
    template <class U> static auto test_clone_const_ref (std::nullptr_t)
      -> decltype(test_clone_const_ref_signature (&U::clone));
    template <class U> static auto test_clone_const_ref (...)->CloneNonExistence;

    using findings_clone_const_ref = decltype(test_clone_const_ref<T> (nullptr));

    template <class R, class U> static auto test_clone_rref_signature (R (U::*) () &&)
      ->CloneExistence<R>;
    template <class U> static auto test_clone_rref (std::nullptr_t)
      -> decltype(test_clone_rref_signature (&U::clone));
    template <class U> static auto test_clone_rref (...)->CloneNonExistence;

    using findings_clone_rref = decltype(test_clone_rref<T> (nullptr));

    template <class R, class U> static auto test_plmt_clone_const_signature (R (U::*) (void *, std::size_t, std::size_t) const)
      ->CloneExistence<R>;
    template <class U> static auto test_plmt_clone_const (std::nullptr_t)
      -> decltype(test_plmt_clone_const_signature (&U::clone));
    template <class U> static auto test_plmt_clone_const (...)->CloneNonExistence;

    using findings_plmt_clone_const = decltype(test_plmt_clone_const<T> (nullptr));

    template <class R, class U> static auto test_plmt_clone_const_ref_signature (R (U::*) (void *, std::size_t, std::size_t) const &)
      ->CloneExistence<R>;
    template <class U> static auto test_plmt_clone_const_ref (std::nullptr_t)
      -> decltype(test_plmt_clone_const_ref_signature (&U::clone));
    template <class U> static auto test_plmt_clone_const_ref (...)->CloneNonExistence;

    using findings_plmt_clone_const_ref = decltype(test_plmt_clone_const_ref<T> (nullptr));

    template <class R, class U> static auto test_plmt_clone_rref_signature (R (U::*) (void *, std::size_t, std::size_t) && )
      ->CloneExistence<R>;
    template <class U> static auto test_plmt_clone_rref (std::nullptr_t)
      -> decltype(test_plmt_clone_rref_signature (&U::clone));
    template <class U> static auto test_plmt_clone_rref (...)->CloneNonExistence;

    using findings_plmt_clone_rref = decltype(test_plmt_clone_rref<T> (nullptr));


  public:
    static constexpr bool has_clone_const = findings_clone_const::value;
    static constexpr bool has_clone_const_ref = findings_clone_const_ref::value;
    static constexpr bool has_clone_rref = findings_clone_rref::value;
    static constexpr bool has_placement_clone_const = findings_plmt_clone_const::value;
    static constexpr bool has_placement_clone_const_ref = findings_plmt_clone_const_ref::value;
    static constexpr bool has_placement_clone_rref = findings_plmt_clone_rref::value;

    static constexpr unsigned char mask =
      (has_clone_const ? (1u << kCloneConst) : 0u) |
      (has_clone_const_ref ? (1u << kCloneConstRef) : 0u) |
      (has_clone_rref ? (1u << kCloneRref) : 0u) |
      (has_placement_clone_const ? (1u << kPlacementCloneConst) : 0u) |
      (has_placement_clone_const_ref ? (1u << kPlacementCloneConstRef) : 0u) |
      (has_placement_clone_rref ? (1u << kPlacementCloneRref) : 0u);

    using return_type_clone_const = typename findings_clone_const::return_type;
    using return_type_clone_const_ref = typename findings_clone_const_ref::return_type;
    using return_type_clone_rref = typename findings_clone_rref::return_type;
    using return_type_placement_clone_const = typename findings_plmt_clone_const::return_type;
    using return_type_placement_clone_const_ref = typename findings_plmt_clone_const_ref::return_type;
    using return_type_placement_clone_rref = typename findings_plmt_clone_rref::return_type;
  };

  template <class R, class ... I>
  class imoveable : public I ... {
  public:
    imoveable () = default;
    imoveable (imoveable const &) = default;
    imoveable (imoveable &&) = default;
    imoveable & operator = (imoveable const &) = default;
    imoveable & operator = (imoveable &&) = default;
    virtual ~imoveable () = default;

    virtual R * clone () && = 0;
    virtual R * clone (void * placement, std::size_t len, std::size_t align) && = 0;
  };


  template <class R, class ... I>
  class icloneable : public imoveable<R, I ...> {
  public:
    icloneable () = default;
    icloneable (icloneable const &) = default;
    icloneable (icloneable &&) = default;
    icloneable & operator = (icloneable const &) = default;
    icloneable & operator = (icloneable &&) = default;
    virtual ~icloneable () = default;

    // needed as otherwise CloneAnalyzer will not detect these methods in VC++ 15
    R * clone () && override = 0;
    R * clone (void * placement, std::size_t len, std::size_t align) && override = 0;

    virtual R * clone () const & = 0;
    virtual R * clone (void * placement, std::size_t len, std::size_t align) const & = 0;
  };

  enum class CopyMove { Both = 0, NoCopy = 1, None = 3 };

  template <class T>
  constexpr CopyMove static_copy_move_capabilities () {
    return std::is_copy_constructible<T>::value ? CopyMove::Both :
      std::is_move_constructible<T>::value ? CopyMove::NoCopy : CopyMove::None;
  }

  template <class ... I>
  class icloneable_extension
    : public icloneable<icloneable_extension<I ...>, I ...> {
  };

  template <class ... I>
  class imoveable_extension
    : public imoveable<imoveable_extension<I ...>, I ...> {
  };

  template <class ... I>
  class isimple_combination
    : public I ... {
  };


  template <CopyMove copyMove, class ... I> using icopy_move_extended =
    std::conditional_t<copyMove == CopyMove::Both, icloneable_extension<I ...>,
    std::conditional_t<copyMove == CopyMove::NoCopy, imoveable_extension<I ...>,
    std::conditional_t<copyMove == CopyMove::None, isimple_combination<I ...>,
    void
    > > >;



  constexpr int getHighestBitPos (unsigned char mask, int upto = 7) {
    for (int i = upto; i != -1; --i) {
      if (mask & (1u << i)) return i;
    }
    return -1;
  }

  static_assert(getHighestBitPos (1) == 0);
  static_assert(getHighestBitPos (128) == 7);
  static_assert(getHighestBitPos (5) == 2);
  static_assert(getHighestBitPos (128, 6) == -1);
  static_assert(getHighestBitPos (0) == -1);

  template <class D, class B, int HighestBit = getHighestBitPos (CloneAnalyzer<B>::mask)> struct CloneImplementation;
  template <class D, class B> using MyCloneImplementation = CloneImplementation<D, B>;

  template<class D, class B>
  struct CloneImplementation<D, B, kCloneRref> : 
    CloneImplementation<D, B, getHighestBitPos(CloneAnalyzer<B>::mask, kCloneRref - 1)>
  {
    using CloneImplementation<D, B, getHighestBitPos (CloneAnalyzer<B>::mask, kCloneRref - 1)>::CloneImplementation;
  private:
    typename CloneAnalyzer<B>::return_type_clone_rref clone () && override {
      return new D{ static_cast <D &&> (std::move (*this)) };
    }
  };


  template<class D, class B>
  struct CloneImplementation<D, B, kPlacementCloneRref> :
    CloneImplementation<D, B, getHighestBitPos (CloneAnalyzer<B>::mask, kPlacementCloneRref - 1)>
  {
    using CloneImplementation<D, B, getHighestBitPos (CloneAnalyzer<B>::mask, kPlacementCloneRref - 1)>::CloneImplementation;
  private:
    typename CloneAnalyzer<B>::return_type_placement_clone_rref  clone (void * placement, std::size_t len, std::size_t align) && override {
      if (placement != nullptr
        && sizeof (D) <= len
        && alignof(D) <= align
        ) {
        new (placement) D{ static_cast <D &&> (std::move (*this)) };
        return static_cast<D *> (placement);
      }
      else {
        return new D{ static_cast <D &&> (std::move (*this)) };
      }
    }
  };

  template<class D, class B>
  struct CloneImplementation<D, B, kCloneConstRef> :
    CloneImplementation<D, B, getHighestBitPos (CloneAnalyzer<B>::mask, kCloneConstRef - 1)>
  {
    using CloneImplementation<D, B, getHighestBitPos (CloneAnalyzer<B>::mask, kCloneConstRef - 1)>::CloneImplementation;
  private:
    typename CloneAnalyzer<B>::return_type_clone_const_ref  clone () const & override {
      return new D{ static_cast <D const &> (*this) };
    }
  };

  template<class D, class B>
  struct CloneImplementation<D, B, kPlacementCloneConstRef> :
    CloneImplementation<D, B, getHighestBitPos (CloneAnalyzer<B>::mask, kPlacementCloneConstRef - 1)>
  {
    using CloneImplementation<D, B, getHighestBitPos (CloneAnalyzer<B>::mask, kPlacementCloneConstRef - 1)>::CloneImplementation;
  private:
    typename CloneAnalyzer<B>::return_type_placement_clone_const_ref  clone (void * placement, std::size_t len, std::size_t align) const & override {
      if (placement != nullptr
        && sizeof (D) <= len
        && alignof(D) <= align
        ) {
        new (placement) D{ static_cast <D const &> (*this) };
        return static_cast<D *> (placement);
      }
      else {
        return new D{ static_cast <D const &> (*this) };
      }
    }
  };

  template<class D, class B>
  struct CloneImplementation<D, B, kCloneConst> :
    CloneImplementation<D, B, getHighestBitPos (CloneAnalyzer<B>::mask, kCloneConst - 1)>
  {
    using CloneImplementation<D, B, getHighestBitPos (CloneAnalyzer<B>::mask, kCloneConst - 1)>::CloneImplementation;
  private:
    typename CloneAnalyzer<B>::return_type_clone_const clone () const override {
      return new D{ static_cast <D const &> (*this) };
    }
  };

  template<class D, class B>
  struct CloneImplementation<D, B, kPlacementCloneConst> :
    CloneImplementation<D, B, getHighestBitPos (CloneAnalyzer<B>::mask, kPlacementCloneConst - 1)>
  {
    using CloneImplementation<D, B, getHighestBitPos (CloneAnalyzer<B>::mask, kPlacementCloneConst - 1)>::CloneImplementation;
  private:
    typename CloneAnalyzer<B>::return_type_placementCloneConst clone (void * placement, std::size_t len, std::size_t align) const override {
      if (placement != nullptr
        && sizeof (D) <= len
        && alignof(D) <= align
        ) {
        new (placement) D{ static_cast <D const &> (*this) };
        return static_cast<D *> (placement);
      }
      else {
        return new D{ static_cast <D const &> (*this) };
      }
    }
  };

  template<class D, class B>
  struct CloneImplementation<D, B, -1> : B {
    using B::B;
  };


  template <class R>
  struct CloneRrefImplementation {
    template<class D, class B>
    class implementation : public B {
    public:
      using B::B;
    private:
      R clone () && override {
        return new D{ static_cast <D &&> (std::move (*this)) };
      }
    };
  };

  template <class R>
  struct PlacementCloneRrefImplementation {
    template<class D, class B>
    class implementation : public B {
    public:
      using B::B;
    private:
      R clone (void * placement, std::size_t len, std::size_t align) && override {
        if (placement != nullptr
          && sizeof (D) <= len
          && alignof(D) <= align
          ) {
          new (placement) D{ static_cast <D &&> (std::move (*this)) };
          return static_cast<D *> (placement);
        }
        else {
          return new D{ static_cast <D &&> (std::move (*this)) };
        }
      }
    };
  };

  template <class R>
  struct CloneConstRefImplementation {
    template<class D, class B>
    class implementation : public B {
    public:
      using B::B;
    private:
      R clone () const & override {
        return new D{ static_cast <D const &> (*this) };
      }
    };
  };

  template <class R>
  struct PlacementCloneConstRefImplementation {
    template<class D, class B>
    class implementation : public B {
    public:
      using B::B;
    private:
      R clone (void * placement, std::size_t len, std::size_t align) const & override {
        if (placement != nullptr
          && sizeof (D) <= len
          && alignof(D) <= align
          ) {
          new (placement) D{ static_cast <D const &> (*this) };
          return static_cast<D *> (placement);
        }
        else {
          return new D{ static_cast <D const &> (*this) };
        }
      }
    };
  };

  template <class R>
  struct CloneConstImplementation {
    template <class D, class B>
    class implementation : public B {
    public:
      using B::B;
    private:
      R clone () const override {
        return new D{ static_cast <D const &> (*this) };
      }
    };
  };

  template <class R>
  struct PlacementCloneConstImplementation {
    template<class D, class B>
    class implementation : public B {
    public:
      using B::B;
    private:
      R clone (void * placement, std::size_t len, std::size_t align) const override {
        if (placement != nullptr
          && sizeof (D) <= len
          && alignof(D) <= align
          ) {
          new (placement) D{ static_cast <D const &> (*this) };
          return static_cast<D *> (placement);
        }
        else {
          return new D{ static_cast <D const &> (*this) };
        }
      }
    };
  };




  template <class T>
  constexpr auto determine_clone_member_decorations (metaprog::type<T> t) {
    using metaprog::Append;
    using metaprog::Identity;
    using metaprog::static_ternary;

    using return_type_clone_const = typename CloneAnalyzer<T>::return_type_clone_const;
    using return_type_clone_const_ref = typename CloneAnalyzer<T>::return_type_clone_const_ref;
    using return_type_clone_rref = typename CloneAnalyzer<T>::return_type_clone_rref;
    using return_type_placement_clone_const = typename CloneAnalyzer<T>::return_type_placement_clone_const;
    using return_type_placement_clone_const_ref = typename CloneAnalyzer<T>::return_type_placement_clone_const_ref;
    using return_type_placement_clone_rref = typename CloneAnalyzer<T>::return_type_placement_clone_rref;

    return metaprog::template_list <> {}
    | Append<MyCloneImplementation> {};
    /*
      | static_ternary <CloneAnalyzer<T>::has_clone_const> (
        Append<CloneConstImplementation<return_type_clone_const>::template implementation> {},
        Identity{})
      | static_ternary <CloneAnalyzer<T>::has_clone_const_ref> (
        Append<CloneConstRefImplementation<return_type_clone_const_ref>::template implementation> {},
        Identity{})
      | static_ternary <CloneAnalyzer<T>::has_clone_rref> (
        Append<CloneRrefImplementation<return_type_clone_rref>::template implementation> {},
        Identity{})
      | static_ternary <CloneAnalyzer<T>::has_placement_clone_const> (
        Append<PlacementCloneConstImplementation<return_type_placement_clone_const>::template implementation> {},
        Identity{})
      | static_ternary <CloneAnalyzer<T>::has_placement_clone_const_ref> (
        Append<PlacementCloneConstRefImplementation<return_type_placement_clone_const_ref>::template implementation> {},
        Identity{})
      | static_ternary <CloneAnalyzer<T>::has_placement_clone_rref> (
        Append<PlacementCloneRrefImplementation<return_type_placement_clone_rref>::template implementation> {},
        Identity{});*/
  }

}