#pragma once

namespace ducktype {
  namespace meta {

    // type <T>
    template <class T> struct type { };

    template <class T, class Op>
    constexpr auto operator | (type<T> t, Op op) {
      return op.apply_to (t);
    }

    template <template <class ...> class> struct type_template {};

    // untype: metafunction removing type < >
    namespace detail {
      template <class T> struct untype_impl;
      template <class T> struct untype_impl<type<T>> { using type = T; };
    }
    template <class T> using untype = typename detail::untype_impl<T>::type;

    namespace detail {
      template <class T, class ... B> struct untype_template_impl;
      template <template <class> class T, class ... B> struct untype_template_impl<type_template<T>, B ...> {
        using type = typename T<B ...>;
      };
    }
    template <class T, class ... B> using untype_template = typename detail::untype_template_impl<T, B ...>::type;

    template <template <class ...> class T, class Op>
    constexpr auto operator | (type_template<T> t, Op op) -> decltype (op.apply_to (t)) {
      return op.apply_to (t);
    }


    // TypeOp: base class for operations on type < > values
    //   derived classes must define an ApplyTo nested template
    template <class Derived>
    struct TypeOp {
      template <class T>
      constexpr auto apply_to (type<T>) const {
        return type< Derived::template ApplyTo<T> > {};
      }
    };

    template <class Derived>
    struct TemplateTypeOp {
      template <template <class ...> class T>
      constexpr auto apply_to (type_template<T>) const {
        return type< Derived::template ApplyTo<T> > {};
      }
    };

    template <class ... B>
    struct SpecializeOp
      : TemplateTypeOp <SpecializeOp<B...>>
    {
      template <template <class ...> class T>
      using ApplyTo = typename T<B ...>;
    };

    // AddInterfaceOp: add an interface to a class by deriving from both the class and the interface,
    //   inheriting all constructors of the class
    template <class I>
    struct AddInterfaceOp
      : TypeOp< AddInterfaceOp<I> >
    {
      template <class B>
      struct ApplyTo : B, I {
        using B::B;
      };
    };


    template <class Extension>
    struct ExtendCrtp
    {
      template <class T>
      struct wrap {
        template <class D> using extended = typename Extension::template extension<T, D>;
      };
      template <class T>
      constexpr auto apply_to (type<T>) const {
        return type_template < wrap<T>::template extended > {};
      }

      template <template <class ...> class T>
      struct wrap2 {
        template <class D, class ... P>
        struct extended_impl {
          using type = typename Extension::template extension<T <P ...>, D>;
        };

        template <class ... P>
        using extended = typename extended_impl<P ...>::type;
      };

      template <template <class ...> class T>
      constexpr auto apply_to (type_template<T>) const {
        return type_template < wrap2<T>::template extended > {};
      }

    };

    
    struct ApplyCrtp : TemplateTypeOp<ApplyCrtp> {
      template <template <class> class T> struct Crtp : T<Crtp<T>> { using base = T<Crtp>; using T<Crtp>::T<Crtp>; };

      template <template <class> class T>
      constexpr auto apply_to (type_template<T>) const { return type<Crtp<T>> {}; }
    };

    template <class ... P>
    struct SpecializeAllButFirst {
      template <template <class ...> class T>
      struct Specialized {
        template <class D>
        using type = typename T<D, P ...>;
      };

      template <template <class ...> class T>
      constexpr auto apply_to (type_template<T>) const {
        using specialized = Specialized<T>;
        return type_template<specialized::template type> {};
      }
    };
  }
}