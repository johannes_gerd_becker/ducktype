#pragma once

#include <memory>
#include <type_traits>
#include <utility>
#include "implement.h"

namespace ducktype {


  namespace detail {

    template <std::size_t Len, std::size_t Align>
    struct statdyn_static_storage {
      static constexpr std::size_t const kLen = Len;
      static constexpr std::size_t const kAlign = Align;

      template <class Z> static constexpr bool can_embed() {
        return can_embed(sizeof(Z), alignof(Z));
      }

      static constexpr bool can_embed(std::size_t len, std::size_t align) {
        return (len <= kLen) && (align <= kAlign);
      }

      void * ptr() { return &m_storage; }
      void const * ptr() const { return &m_storage; }

    private:
      std::aligned_storage_t<Len, Align> m_storage;
    };

    template <std::size_t Align>
    struct statdyn_static_storage<0,Align> {
      static constexpr std::size_t const kLen = 0;
      static constexpr std::size_t const kAlign = Align;

      template <class Z> static constexpr bool can_embed() { return false; }
      static constexpr bool can_embed(std::size_t /* len */, std::size_t /* align */) { return false; }

      void * ptr() { return nullptr; }
      void const * ptr() const { return nullptr; }
    };

    template <class GetType, std::size_t Len, std::size_t Align>
    class statdyn
      : private statdyn_static_storage<Len,Align>
    {
      static_assert(Align >= 1, "Align must be >= 1");

      template <class, std::size_t, std::size_t> friend class statdyn;
    private:
      using storage = statdyn_static_storage<Len, Align>;

    public:
      // emplace construction
      template <class SetType, class ... Args>
      statdyn(SetType *, bool forcedynamic, Args && ... args) {
        if (can_embed<SetType>() && !forcedynamic) {
          new (storage::ptr()) SetType{ std::forward<Args>(args) ... };
          m_ptr = reinterpret_cast<SetType *> (storage::ptr());
        } else {
          m_ptr = new SetType{ std::forward<Args>(args) ... };
        }
      }

      // copy construction
      statdyn(statdyn const & other) {
        m_ptr = other.get().clone(enriched_tag_t{}, storage::ptr(), kLen, kAlign);
      }

      template <std::size_t L, std::size_t A>
      statdyn(statdyn<GetType, L, A> const & other) {
        m_ptr = other.get().clone(enriched_tag_t{}, storage::ptr(), kLen, kAlign);
      }

      // move construction
      statdyn(statdyn && other) {
        if (other.does_embed()) {
          m_ptr = std::move(other).get().clone(enriched_tag_t{}, storage::ptr(), kLen, kAlign);
        } else {
          m_ptr = other.m_ptr; other.m_ptr = nullptr;
        }
      }

      template <std::size_t L, std::size_t A>
      statdyn(statdyn<GetType, L, A> && other) {
        if (other.does_embed()) {
          m_ptr = std::move(other).get().clone(enriched_tag_t{}, storage::ptr(), kLen, kAlign);
        } else {
          m_ptr = other.m_ptr; other.m_ptr = nullptr;
        }
      }

      // copy assignment
      statdyn & operator = (statdyn const & other) {
        auto tmp = statdyn{ other };
        *this = std::move(tmp);
        return *this;
      }
      
      template <std::size_t L, std::size_t A>
      statdyn & operator = (statdyn<GetType, L, A> const & other) {
        auto tmp = statdyn{ other };
        *this = std::move(tmp);
        return *this;
      }

      // move assignment
      statdyn & operator = (statdyn && other) {
        destruct();
        if (other.does_embed()) {
          m_ptr = std::move(other.get()).clone(enriched_tag_t{}, storage::ptr(), kLen, kAlign);
        } else {
          m_ptr = other.m_ptr; other.m_ptr = nullptr;
        }
      }

      template <std::size_t L, std::size_t A>
      statdyn & operator = (statdyn<GetType, L, A> && other) {
        destruct();
        if (other.does_embed()) {
          m_ptr = std::move(other.get()).clone(enriched_tag_t{}, storage::ptr(), kLen, kAlign);
        } else {
          m_ptr = other.m_ptr; other.m_ptr = nullptr;
        }
      }

      // destructor
      ~statdyn() { destruct(); }

      // get
      GetType & get() & { return *m_ptr; }
      GetType const & get() const & { return *m_ptr; }
      GetType && get() && { return std::move(*m_ptr); }

      // release_ptr
      GetType * release_ptr() && {
        auto && tmp = does_embed() ?
          statdyn<GetType, 0, 1> { *this }
        :
          *this;
        auto ptr = tmp.m_ptr;
        tmp.m_ptr = nullptr;
        return ptr;
      }

    private:
      bool does_embed() const {
        return (char *)storage::ptr() <= (char *)m_ptr && (char *)m_ptr < (char *)storage::ptr() + kLen;
      }

      void destruct() {
        if (does_embed()) {
          m_ptr->~GetType();
        } else {
          delete m_ptr;
        }
        m_ptr = nullptr;
      }

    private:
      GetType * m_ptr;
    };
  }

  struct object_holder_emplace_tag_t { constexpr object_holder_emplace_tag_t() {} };
  static constexpr object_holder_emplace_tag_t const object_holder_emplace_tag = object_holder_emplace_tag_t{};
  struct create_forceembed_tag_t { constexpr create_forceembed_tag_t() {} };
  static constexpr create_forceembed_tag_t const create_forceembed_tag = create_forceembed_tag_t{};
  struct create_noembed_tag_t { constexpr create_noembed_tag_t() {} };
  static constexpr create_noembed_tag_t const create_noembed_tag = create_noembed_tag_t{};

  template <
    class I,
    std::size_t Len = 3 * sizeof(void *),
    std::size_t Align = alignof(I)
  >
  class ducktyped {
  private:
    using InterfaceType = I;
    template <class T> using ImplementationType = implementation<I,T>;

  public:
    template <class T>
    ducktyped(T && t, object_holder_emplace_tag_t)
      : m_statdyn{ (ImplementationType<T> *) 0, false, std::forward<T>(t) }
    { }

    template <class T>
    ducktyped(T && t, create_noembed_tag_t)
      : m_statdyn{ (ImplementationType<T> *) 0, true, std::forward<T>(t) }
    { }

    template <class T>
    ducktyped(T && t, create_forceembed_tag_t)
      : ducktyped(std::forward<T> (t), object_holder_emplace_tag_t)
    {
      static_assert(decltype(m_statdyn)::can_embed<T>(), "The passed type does not satisfy the requirements for embedding.");
    }

    InterfaceType & operator * () & { return m_statdyn.get(); }
    InterfaceType const & operator * () const & { return m_statdyn.get(); }
    InterfaceType & operator * () && { return m_statdyn.get(); }

    InterfaceType * operator -> () & { return &m_statdyn.get(); }
    InterfaceType const * operator -> () const & { return &m_statdyn.get(); }
    InterfaceType * operator -> () && { return &m_statdyn.get(); }

    operator InterfaceType & () & { return m_statdyn.get(); }
    operator InterfaceType const & () const & { return m_statdyn.get(); }
    operator InterfaceType && () && { return std::move(m_statdyn).get(); }

    std::unique_ptr<I> to_unique() && {
      return std::unique_ptr<I> { std::move(m_statdyn).release_ptr() };
    }

    ~ducktyped() = default;
  private:
    detail::statdyn<cloneable_enriched<InterfaceType>, Len, Align> m_statdyn;
  };
}