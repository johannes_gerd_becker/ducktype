#pragma once

#include "metaprog/fundamentals.h"
#include "metaprog/staticif.h"
#include "metaprog/type_operations.h"
#include "duck/member_clone.h"
#include "duck/object_holder.h"

#include <memory>
#include <type_traits>

namespace ducktype {

  // We provide the following functions:
  // * adapt<Adaptor> ---> don't inherit
  // * icopy_move_extended metafunction ---> extends interface by clone methods
  // * implement<Interface> ---> inherit, automatically implement clone members
  // * decorate<Decoration> ---> inherit


  template <
    template <class, class> class Adaptor,
    class Outer,
    class AccessFunctional = typename ducktype::access_traits<std::decay_t<Outer>>::functional
  >
  constexpr auto adapt (Outer && outer) {
	  using metaprog::type;
	  using metaprog::type_template;
	  using metaprog::decorate;
	  using metaprog::construct;

    using outer_type = std::remove_cv_t<std::decay_t<Outer>>;

    auto iba = decorate (type< object_holder<outer_type, AccessFunctional> >{}, type_template<Adaptor> {});

    return construct (iba, object_holder_emplace_tag{}, std::forward<Outer> (outer));
  }


  template <
    template <class, class> class Decoration,
    class Outer,
    class AccessFunctional = typename ducktype::access_traits<std::decay_t<Outer>>::functional
  >
  constexpr auto decorate (Outer && outer) {
    using OuterType = std::remove_cv_t<std::decay_t<Outer>>;

    auto holder_type = metaprog::type< object_holder<OuterType, AccessFunctional> >{};
    auto decoration_type = metaprog::type_template<Decoration>{};
    auto result_type = metaprog::decorate (holder_type, decoration_type);

    return metaprog::construct (result_type, std::forward<Outer> (outer));
  }


  namespace detail {
    template <class ImplementationTraits, class Outer>
    struct implementation_of {
      template <class D, class B>
      using templ = typename ImplementationTraits::template implementation<Outer, D, B>;
    };

    template <class Outer, class AccessFunctional, class I>
    struct augmented_object_holder : object_holder<Outer, AccessFunctional>, I {
      using object_holder<Outer, AccessFunctional>::object_holder;
    };
  }

  template <
    class ImplementationTraits,
    class Outer,
    class AccessFunctional = typename ducktype::access_traits<std::decay_t<Outer>>::functional
  >
  auto force_implement (Outer && outer, AccessFunctional = AccessFunctional{}) {

    constexpr auto determine_implementation_type = []() {
      using raw_inner_type = decltype (AccessFunctional{} (std::declval<Outer> ()));
      using inner_type = std::remove_cv_t<std::decay_t<raw_inner_type>>;
      using outer_type = std::remove_cv_t<std::decay_t<Outer>>;

      using interface = typename ImplementationTraits::interface;

      auto augmented_holder_type = metaprog::type<detail::augmented_object_holder<outer_type, AccessFunctional, interface>>{};
      
      auto decorations = determine_clone_member_decorations (augmented_holder_type)
        | metaprog::Append<detail::implementation_of<ImplementationTraits, inner_type>::template templ>{};

      return metaprog::decorate_type (augmented_holder_type, decorations);
    };

    return metaprog::construct (determine_implementation_type (), object_holder_emplace_tag{}, std::forward<Outer> (outer));

  }

  template <
    class ImplementationTraits,
    class Outer,
    class AccessFunctional = typename ducktype::access_traits<std::decay_t<Outer>>::functional
  >
  auto implement (Outer outer, AccessFunctional = AccessFunctional{}) {
    using intf = typename std::remove_const_t<ImplementationTraits>::interface;
    using intf_maybe_const = std::conditional_t<std::is_const<ImplementationTraits>::value, intf const, intf>;

    if constexpr (std::is_convertible_v<Outer &, intf_maybe_const &>)
      return std::move (outer);
    else
      return force_implement<intf> (std::move (outer));
  }

  template <
    class ImplementationTraits,
    class Outer,
    class AccessFunctional = typename ducktype::access_traits<std::decay_t<Outer>>::functional
  >
  auto as_ref (Outer & outer, AccessFunctional = AccessFunctional{}) {
    using intf = typename std::remove_const_t<ImplementationTraits>::interface;
    using intf_maybe_const = std::conditional_t<std::is_const<ImplementationTraits>::value, intf const, intf>;
    return metaprog::static_if<std::is_convertible<Outer &, intf_maybe_const &>::value> (
      [&](auto, auto ... nothing) { return std::reference_wrapper<intf_maybe_const> (outer, nothing ...); },
      [&](auto, auto ... nothing) { return force_implement<intf> (std::ref (outer, nothing ...)); }
    );
  }

  template <
    class ImplementationTraits,
    class Outer,
    class AccessFunctional = typename ducktype::access_traits<std::decay_t<Outer>>::functional
  >
  auto as_ref (Outer const & outer, AccessFunctional = AccessFunctional{}) {
    using intf = typename std::remove_const_t<ImplementationTraits>::interface;
    return metaprog::static_if<std::is_convertible<Outer const &, intf const &>::value> (
      [&](auto, auto ... nothing) { return std::reference_wrapper<intf const> (outer, nothing ...); },
      [&](auto, auto ... nothing) { return force_implement<intf> (std::cref (outer, nothing ...)); }
    );
  }

  template <
    class ImplementationTraits,
    class Outer,
    class AccessFunctional = typename ducktype::access_traits<std::decay_t<Outer>>::functional
  >
  auto as_ref (Outer const &&, AccessFunctional = AccessFunctional{})
    = delete;

/*
  template <class Interface, class T, class ... Args>
  auto construct_implement(Args && ... args)
    -> implementation < Interface, T >
  {
    return implementation<Interface, T> { object_holder_emplace_tag{}, std::forward<T>(args) ... };
  }

  template <class Interface, class T, class ... Args>
  auto placement_implement(void * loc, Args && ... args)
    -> implementation < Interface, T > *
  {
    return new (loc) implementation < Interface, T > { object_holder_emplace_tag{}, std::forward<Args>(args) ... };
  }
*/
}