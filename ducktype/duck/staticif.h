#ifndef METAPROG_STATICIF_H_JSQ3W9OTOXZD2M7PLDQO
#define METAPROG_STATICIF_H_JSQ3W9OTOXZD2M7PLDQO

#pragma once

#include <type_traits>

namespace metaprog {
	
	//
	// bool_c
	//
	
	template <int I> constexpr std::integral_constant<bool,!!I> bool_c () { return {}; }
	
	
	//
	// static_if
	//
	
	namespace detail {
    struct noop {
      template <class X>
      constexpr decltype(auto) operator () (X && x) const {
        return std::forward<X> (x);
      }
    };

		template <class F>
		constexpr decltype(auto) static_if_branch_call_ (F && f) {
			return std::forward<F> (f) (
        noop{}
			);
		}
	}
	
	// two-argument version
	
	template <class F>
  constexpr decltype(auto) static_if (std::true_type, F && f) {
		return detail::static_if_branch_call_ (std::forward<F> (f));
	}	
	
	template <class F>
  constexpr void static_if (std::false_type, F &&) {
	}
	
	// three-argument version
	
	template <class F, class G>
  constexpr decltype(auto) static_if (std::true_type, F && f, G &&) {
		return detail::static_if_branch_call_ (std::forward<F> (f));
	}	
	
	template <class F, class G>
  constexpr decltype(auto) static_if (std::false_type, F &&, G && g) {
		return detail::static_if_branch_call_ (std::forward<G> (g));
	}

	// four-or-more-argument version

	template <class F, bool B, class Other, class ... Others>
  constexpr decltype(auto) static_if (std::true_type, F && f, std::integral_constant<bool,B>, Other &&, Others && ...) {
		return detail::static_if_branch_call_ (std::forward<F> (f));
	}

	template <class F, bool B, class Other, class ... Others>
  constexpr decltype(auto) static_if (std::false_type, F &&, std::integral_constant<bool,B> b, Other && other, Others && ... others) {
		return static_if (b, std::forward<Other> (other), std::forward<Others> (others) ...);
	}

	// angular brackets version: static_if <bool (or integer)> (if_lambda, else_lambda)
	
	template <long long B, class ... Args>
	constexpr decltype(auto) static_if (Args && ... args) {
		return static_if (bool_c <B> (), std::forward<Args> (args) ...);
	}


	
	
}

#endif